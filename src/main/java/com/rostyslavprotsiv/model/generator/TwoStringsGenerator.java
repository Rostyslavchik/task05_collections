package com.rostyslavprotsiv.model.generator;

import com.rostyslavprotsiv.model.entity.TwoStrings;

import java.util.Arrays;
import java.util.List;

public class TwoStringsGenerator {
    private static final int SIZE = 8;

    public TwoStrings[] generateArray() {
        TwoStrings[] generated = new TwoStrings[SIZE];
        for(int i = 0; i < SIZE; i++) {
            generated[i] = new TwoStrings();
        }
        generated[0].setFirst("Namibia");
        generated[0].setSecond("Windhoek");
        generated[1].setFirst("India");
        generated[1].setSecond("New Delhi");
        generated[2].setFirst("Malawi");
        generated[2].setSecond("Lilongwe");
        generated[3].setFirst("Liberia");
        generated[3].setSecond("Monrovia");
        generated[4].setFirst("Xhinaa");
        generated[4].setSecond("Beijing");
        generated[5].setFirst("Mexico");
        generated[5].setSecond("Mexico City");
        generated[6].setFirst("Pakistan");
        generated[6].setSecond("Islamabad");
        generated[7].setFirst("Oman");
        generated[7].setSecond("Muscat");
        return generated;
    }

    public List<TwoStrings> generateList() {
        return Arrays.asList(generateArray());
    }
}
