package com.rostyslavprotsiv.model.entity;

import java.util.*;

public class MyTreeMap<K, V> extends AbstractMap<K, V>
        implements NavigableMap<K, V> {
    //here we will not use Comparator(only Comparable), used AVL-tree
    //After each put\remove operation we also should rebalance the tree
    private Entry<K, V> root = null;
    private int size = 0;

    public MyTreeMap() {}

    @Override
    public int size() {
        return size;
    }

    @Override
    public V get(Object key) {
        Entry<K, V> p = getEntry(key);
        return (p == null ? null : p.value);
    }

    private final Entry<K, V> getEntry(Object key) {
        @SuppressWarnings("unchecked")
        Comparable<? super K> k = (Comparable<? super K>) key;
        Entry<K, V> p = root;
        while (p != null) {
            int cmp = k.compareTo(p.key);
            if (cmp < 0) {
                p = p.left;
            } else if (cmp > 0) {
                p = p.right;
            } else {
                return p;
            }
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        Entry<K, V> t = root;
        if (t == null) {
            compare(key, key);
            root = new Entry(key, value, null);
            size = 1;
            return null;
        }
        int cmp;
        Entry<K, V> parent;
        if (key == null) {
            throw new NullPointerException();
        }
        @SuppressWarnings("unchecked")
                Comparable<? super K> k = (Comparable<? super K>) key;
        do {
            parent = t;
            cmp = k.compareTo(t.key);
            if (cmp < 0) {
                t = t.left;
            } else if (cmp > 0) {
                t = t.right;
            } else {
                return t.setValue(value);
            }
        } while (t != null);
        Entry<K, V> e = new Entry<>(key, value, parent);
        if (cmp < 0) {
            parent.left = e;
        } else {
            parent.right = e;
        }
//        balance();
        size++;
        return null;
    }

    @Override
    public V remove(Object key) {
        Entry<K, V> p = getEntry(key);
        if (p == null) {
            return null;
        }
        V oldValue = p.value;
        deleteEntry(p);
        return oldValue;
    }

    private void deleteEntry(Entry<K, V> p) {
        size--;
        if (p.right == null && p.left == null) {
            deleteWithoutChildren(p);
        }

//        } else if ()...
    }

    private void deleteWithoutChildren(Entry<K, V> p) {
        Entry<K, V> parent = p.parent;
        int cmp;
        @SuppressWarnings("unchecked")
        Comparable<? super K> k = (Comparable<? super K>) p.getKey();
        if (p == root) {
            root = null;
        } else if (parent != null) {
            cmp = k.compareTo(parent.key);
            if (cmp < 0) {
                parent.left = null;
            } else {
                parent.right = null;
            }
        }
        p.parent = null;
    }

    @SuppressWarnings("unchecked")
    final int compare(Object k1, Object k2) {
        return ((Comparable<? super K>) k1).compareTo((K)k2);
    }

    static final class Entry<K, V> implements Map.Entry<K, V> {
        private K key;
        private V value;
        Entry<K, V> left = null;
        Entry<K, V> right = null;
        Entry<K, V> parent = null;

        Entry(K key, V value, Entry<K, V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V v) {
            V oldValue = this.value;
            this.value = v;
            return oldValue;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Entry<?, ?> entry = (Entry<?, ?>) o;
            return Objects.equals(key, entry.key)
                    && Objects.equals(value, entry.value)
                    && Objects.equals(left, entry.left)
                    && Objects.equals(right, entry.right)
                    && Objects.equals(parent, entry.parent);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value, left, right, parent);
        }

        @Override
        public String toString() {
            return "Entry{" +
                    "key=" + key +
                    ", value=" + value +
                    ", left=" + left +
                    ", right=" + right +
                    '}';
        }
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return null;
    }

    @Override
    public V putIfAbsent(K k, V v) {
        return super.putIfAbsent(k, v);
    }

    @Override
    public boolean remove(Object o, Object o1) {
        return super.remove(o, o1);
    }

    @Override
    public Entry<K, V> lowerEntry(K k) {
        return null;
    }

    @Override
    public K lowerKey(K k) {
        return null;
    }

    @Override
    public Entry<K, V> floorEntry(K k) {
        return null;
    }

    @Override
    public K floorKey(K k) {
        return null;
    }

    @Override
    public Entry<K, V> ceilingEntry(K k) {
        return null;
    }

    @Override
    public K ceilingKey(K k) {
        return null;
    }

    @Override
    public Entry<K, V> higherEntry(K k) {
        return null;
    }

    @Override
    public K higherKey(K k) {
        return null;
    }

    @Override
    public Entry<K, V> firstEntry() {
        return null;
    }

    @Override
    public Entry<K, V> lastEntry() {
        return null;
    }

    @Override
    public Entry<K, V> pollFirstEntry() {
        return null;
    }

    @Override
    public Entry<K, V> pollLastEntry() {
        return null;
    }

    @Override
    public NavigableMap<K, V> descendingMap() {
        return null;
    }

    @Override
    public NavigableSet<K> navigableKeySet() {
        return null;
    }

    @Override
    public NavigableSet<K> descendingKeySet() {
        return null;
    }

    @Override
    public NavigableMap<K, V> subMap(K k, boolean b, K k1, boolean b1) {
        return null;
    }

    @Override
    public NavigableMap<K, V> headMap(K k, boolean b) {
        return null;
    }

    @Override
    public NavigableMap<K, V> tailMap(K k, boolean b) {
        return null;
    }

    @Override
    public Comparator<? super K> comparator() {
        return null;
    }

    @Override
    public SortedMap<K, V> subMap(K k, K k1) {
        return null;
    }

    @Override
    public SortedMap<K, V> headMap(K k) {
        return null;
    }

    @Override
    public SortedMap<K, V> tailMap(K k) {
        return null;
    }

    @Override
    public K firstKey() {
        return null;
    }

    @Override
    public K lastKey() {
        return null;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Entry<K, V> p = root;
        result.append(p);
        return result.toString();
    }
}
