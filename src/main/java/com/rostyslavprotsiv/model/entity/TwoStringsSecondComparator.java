package com.rostyslavprotsiv.model.entity;

import java.util.Comparator;

public class TwoStringsSecondComparator implements Comparator<TwoStrings> {
    @Override
    public int compare(TwoStrings obj1, TwoStrings obj2) {
        if (obj2.getSecond().equals(obj1.getSecond())) {
            return 0;
        } else if (obj2.getSecond().length() < obj1.getSecond().length()) {
            return -1;
        } else {
            return 1;
        }
//        return obj1.getSecond().length() - obj2.getSecond().length();
    }
}
