package com.rostyslavprotsiv.model.entity;

import java.util.Arrays;

public class StringContainer {
    private String[] elementData;
    private static final int DEFAULT_CAPACITY = 16;
    private static final String[] EMPTY_ELEMENTDATA = {};
    private static final int CAPACITY_FACTOR = 2;
    private int size;

    public StringContainer() {
        elementData = EMPTY_ELEMENTDATA;
    }

    public StringContainer(String[] elementData) {
        this.elementData = elementData;
    }

    public String get(int index) {
        rangeCheck(index);
        return elementData[index];
    }

    public boolean add(String str) {
        ensureCapacity(size + 1);
        elementData[size++] = str;
        return true;
    }

    private void ensureCapacity(int minCapacity)  {
        if(elementData == EMPTY_ELEMENTDATA) {
            minCapacity = Math.max(DEFAULT_CAPACITY, minCapacity);
        }
        if (minCapacity - elementData.length > 0) {
            int oldCapacity = elementData.length;
            int newCapacity = oldCapacity + (oldCapacity * CAPACITY_FACTOR);
            if (newCapacity - minCapacity < 0) {
                newCapacity = minCapacity;
            }
            elementData = Arrays.copyOf(elementData, newCapacity);
        }
    }

    private void rangeCheck(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
        }
    }

    private String outOfBoundsMsg(int index) {
        return "Index: " + index + ", Size: " + this.size;
    }
}
