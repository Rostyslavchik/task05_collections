package com.rostyslavprotsiv;

import com.rostyslavprotsiv.model.entity.MyTreeMap;

public class MyTreeMapTest {
    public static void main(String[] args) {
        MyTreeMap<Integer, String> tree = new MyTreeMap<>();
        tree.put(5, "A");
        tree.put(6, "g");
        tree.put(7, "h");
        tree.put(1, "th");
        tree.put(4, "il");
        System.out.println(tree);
        tree.remove(5);
        System.out.println(tree);
    }
}
