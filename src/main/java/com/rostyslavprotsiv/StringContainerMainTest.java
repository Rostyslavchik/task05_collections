package com.rostyslavprotsiv;

import com.rostyslavprotsiv.model.entity.StringContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class StringContainerMainTest {
    public static final Logger LOGGER = LogManager.
            getLogger(StringContainerMainTest.class);
    public static final int MAX_FUNCTION_CALLS = 99999;
    public static final int MAX_OPERATION_CALLS = 999;

    public static void main(String[] args) {
        LOGGER.info("Container add : " + testStringContainerAdd());
        LOGGER.info("List add: " + testStringArrayListAdd());
        LOGGER.info("Container get : " + testStringContainerGet());
        LOGGER.info("List get: " + testStringArrayListGet());
    }

    private static long testStringContainerAdd() {
        StringContainer sc;
        long start, end, difference = 0, average;
        for (int j = 0; j < MAX_OPERATION_CALLS; j ++) {
            sc = new StringContainer();
            start = System.nanoTime();
            for (int i = 0; i < MAX_FUNCTION_CALLS; i++) {
                sc.add("RT");
            }
            end = System.nanoTime();
            difference += (end - start);
        }
        average = difference / MAX_OPERATION_CALLS;
        return average;
    }

    private static long testStringArrayListAdd() {
        List<String> ls;
        long start, end, difference = 0, average;
        for (int j = 0; j < MAX_OPERATION_CALLS; j ++) {
            ls = new ArrayList<>();
            start = System.nanoTime();
            for (int i = 0; i < MAX_FUNCTION_CALLS; i++) {
                ls.add("RT");
            }
            end = System.nanoTime();
            difference += (end - start);
        }
        average = difference / MAX_OPERATION_CALLS;
        return average;
    }

    private static long testStringContainerGet() {
        StringContainer sc;
        long start, end, difference = 0, average;
        for (int j = 0; j < MAX_OPERATION_CALLS; j ++) {
            sc = new StringContainer();
            for (int i = 0; i < MAX_FUNCTION_CALLS; i++) {
                sc.add("RT");
            }
            start = System.nanoTime();
            for (int i = 0; i < MAX_FUNCTION_CALLS; i++) {
                sc.get(i);
            }
            end = System.nanoTime();
            difference += (end - start);
        }
        average = difference / MAX_OPERATION_CALLS;
        return average;
    }

    private static long testStringArrayListGet() {
        List<String> ls;
        long start, end, difference = 0, average;
        for (int j = 0; j < MAX_OPERATION_CALLS; j ++) {
            ls = new ArrayList<>();
            for (int i = 0; i < MAX_FUNCTION_CALLS; i++) {
                ls.add("RT");
            }
            start = System.nanoTime();
            for (int i = 0; i < MAX_FUNCTION_CALLS; i++) {
                ls.get(i);
            }
            end = System.nanoTime();
            difference += (end - start);
        }
        average = difference / MAX_OPERATION_CALLS;
        return average;
    }
}
