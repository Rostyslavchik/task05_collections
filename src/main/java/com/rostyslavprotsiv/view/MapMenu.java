package com.rostyslavprotsiv.view;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MapMenu {
    private Map<String, String> menu;
    private Map<String, IPrintable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MapMenu() {
        initMenu();
        initMethodsMenu();
    }

    public void show() {
        String menuKey;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            menuKey = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(menuKey).print("MenuKeyStr");
            } catch (Exception e) {
                System.out.println("Something happened " + e.getMessage()
                        + " " + e);
            }
        } while (!menuKey.equals("Q"));
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "Test something");
        menu.put("2", "Test Map");
        menu.put("3", "Test you");
        menu.put("4", "Bla bla bla");
        menu.put("5", "Test FuncInterface");
        menu.put("Q", "Q - exit");
    }

    private void initMethodsMenu() {
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::testSomething);
        methodsMenu.put("2", this::testMap);
        methodsMenu.put("3", this::testYou);
        methodsMenu.put("4", this::testBlah);
        methodsMenu.put("5", this::testFuncInterface);
    }

    private void testSomething(String str) {
        System.out.println("Testing something:str - " + str);
    }

    private void testMap(String str) {
        System.out.println("Testing map:str - " + str);
    }

    private void testYou(String str) {
        System.out.println("Don't mess with me:str - " + str);
    }

    private void testBlah(String str) {
        System.out.println("Blah blah blah:str - " + str);
    }

    private void testFuncInterface(String str) {
        System.out.println("Test Functional Interface:str - " + str);
    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for(String str: menu.values()) {
            System.out.println(str);
        }
    }
}
