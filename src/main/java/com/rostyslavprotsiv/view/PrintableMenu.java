package com.rostyslavprotsiv.view;

public enum PrintableMenu {
    A(EnumMenu::testSomething),
    B(EnumMenu::testMap),
    C(EnumMenu::testYou),
    D(EnumMenu::testBlah),
    E(EnumMenu::testFuncInterface);

    private IPrintable printable;
    PrintableMenu(IPrintable printable) {
        this.printable = printable;
    }

    public IPrintable getPrintable() {
        return printable;
    }
}
