package com.rostyslavprotsiv.view;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class EnumMenu {
    private Map<String, String> menu;
    private static Scanner input = new Scanner(System.in);

    public EnumMenu() {
        initMenu();
    }

    public void show() {
        String menuKey;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            menuKey = input.nextLine().toUpperCase();
            try {
                PrintableMenu.valueOf(menuKey).getPrintable().print("MenuKeyStr");
            } catch (Exception e) {
                System.out.println("Something happened " + e.getMessage()
                        + " " + e);
            }
        } while (!menuKey.equals("Q"));
    }

    private void initMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "A - Test something");
        menu.put("2", "B - Test Map");
        menu.put("3", "C - Test you");
        menu.put("4", "D - Bla bla bla");
        menu.put("5", "E - Test FuncInterface");
        menu.put("Q", "Q - exit");
    }

    public static void testSomething(String str) {
        System.out.println("Testing something:str - " + str);
    }

    public static void testMap(String str) {
        System.out.println("Testing map:str - " + str);
    }

    public static void testYou(String str) {
        System.out.println("Don't mess with me:str - " + str);
    }

    public static void testBlah(String str) {
        System.out.println("Blah blah blah:str - " + str);
    }

    public static void testFuncInterface(String str) {
        System.out.println("Test Functional Interface:str - " + str);
    }

    private void outputMenu() {
        System.out.println("\nMenu:");
        for(String str: menu.values()) {
            System.out.println(str);
        }
    }
}
