package com.rostyslavprotsiv.view;

public class Menu {
    public void welcome() {
        System.out.println("Welcome to my program!!");
    }

    public void outFirstCase(String sortedArray, String sortedList) {
        System.out.println("Sorted array with Comparable: " + sortedArray);
        System.out.println("Sorted list with Comparable: " + sortedList);
    }

    public void outSecondCase(String sortedArray, String sortedList,
                              String searchedElement, int indexArr,
                              int indexList) {
        System.out.println("Sorted array with Comparator: " + sortedArray);
        System.out.println("Sorted list with Comparator: " + sortedList);
        System.out.println("(Comparator)Found the element : " + searchedElement
                + " with binarySearch in array:" + indexArr);
        System.out.println("(Comparator)Found the element : " + searchedElement
                + " with binarySearch in list:" + indexList);
    }

}
