package com.rostyslavprotsiv.view;

@FunctionalInterface
public interface IPrintable {
    void print(String str);
}
