package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;
import com.rostyslavprotsiv.model.entity.StringContainer;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.execute();
    }
}
