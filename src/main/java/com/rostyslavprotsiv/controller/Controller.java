package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.Main;
import com.rostyslavprotsiv.model.entity.TwoStrings;
import com.rostyslavprotsiv.model.entity.TwoStringsSecondComparator;
import com.rostyslavprotsiv.model.generator.TwoStringsGenerator;
import com.rostyslavprotsiv.view.Menu;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Controller {
    private static final Menu MENU = new Menu();
    private static final TwoStringsGenerator generator =
            new TwoStringsGenerator();


    public void execute() {
        MENU.welcome();
        sortWithComparable();
        sortWithComparator();
    }

    private void sortWithComparable() {
        TwoStrings[] arr;
        List<TwoStrings> list;
        String sortedArray, sortedList;
        arr = generator.generateArray();
        list = generator.generateList();
        Arrays.sort(arr);
        Collections.sort(list);
        sortedArray = Arrays.toString(arr);
        sortedList = list.toString();
        MENU.outFirstCase(sortedArray, sortedList);
    }

    private void sortWithComparator() {
        TwoStrings[] arr;
        List<TwoStrings> list;
        int indexInArr;
        int indexInList;
        TwoStrings key = new TwoStrings("Namibia", "Windhoek");
        String sortedArray, sortedList;
        arr = generator.generateArray();
        list = generator.generateList();
        Comparator<TwoStrings> comparator = new TwoStringsSecondComparator();
        Arrays.sort(arr, comparator);
        Collections.sort(list, comparator);
        sortedArray = Arrays.toString(arr);
        sortedList = list.toString();
        indexInArr = Arrays.binarySearch(arr, key, comparator);
        indexInList = Collections.binarySearch(list, key, comparator);
        MENU.outSecondCase(sortedArray, sortedList, key.toString(),
                indexInArr, indexInList);
    }
}
